package datastructure;

import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/16
 */
public class ListNodeTest {
	@Test
	public void fromListTest(){
		List<Integer> list = new LinkedList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);

		ListNode head = ListNode.fromList(list);
		head.print();
	}
}
