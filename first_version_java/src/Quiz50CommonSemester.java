import datastructure.TreeNode;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by lix7 on 2019/3/31
 */
public class Quiz50CommonSemester {
	public static void main(String[] args){
		TreeNode root = new TreeNode()
				.setVal(1)
				.setLeft(
						new TreeNode()
								.setVal(2)
								.setRight(
										new TreeNode()
												.setVal(3)
								)
				)
				.setRight(
						new TreeNode()
								.setVal(4)
								.setLeft(
										new TreeNode()
												.setVal(6)
								)
								.setRight(
										new TreeNode()
												.setVal(7)
										.setRight(
												new TreeNode()
												.setVal(8)
										)
								)
				);

		System.out.println(commonSemester(root, new TreeNode().setVal(1), new TreeNode().setVal(1)).getVal());
		System.out.println(commonSemester(root, new TreeNode().setVal(1), new TreeNode().setVal(2)).getVal());
		System.out.println(commonSemester(root, new TreeNode().setVal(2), new TreeNode().setVal(4)).getVal());
		System.out.println(commonSemester(root, new TreeNode().setVal(3), new TreeNode().setVal(8)).getVal());
		System.out.println(commonSemester(root, new TreeNode().setVal(7), new TreeNode().setVal(8)).getVal());
	}

	public static TreeNode commonSemester(TreeNode root, TreeNode a, TreeNode b){
		List<TreeNode> aPath = getPath(root, a);
		List<TreeNode> bPath = getPath(root, b);

		TreeNode preNode = null;
		Iterator<TreeNode> aIter = aPath.iterator();
		Iterator<TreeNode> bIter = bPath.iterator();

		while (true){
			if (! aIter.hasNext() || ! bIter.hasNext()){
				return preNode;
			}

			TreeNode aNode = aIter.next();
			TreeNode bNode = bIter.next();
			if (aNode != bNode){
				return preNode;
			}
			preNode = aNode;
		}
	}

	private static List<TreeNode> getPath(TreeNode root, TreeNode target){
		return getPathImpl(root, target, new LinkedList<>());
	}

	private static List<TreeNode> getPathImpl(TreeNode root, TreeNode target, List<TreeNode> curPath){
		if (root == null){
			return null;
		}

		if (root.getVal() == target.getVal()){
			curPath.add(root);
			return curPath;
		}else {

			curPath.add(root);
			List<TreeNode> leftPath = getPathImpl(root.getLeft(), target, curPath);
			if (leftPath != null){
				return leftPath;
			}

			List<TreeNode> rightPath = getPathImpl(root.getRight(), target, curPath);
			if (rightPath != null){
				return rightPath;
			}

			curPath.remove(root);
		}

		return null;
	}

}
