/**
 * Created by lix7 on 2019/2/13
 */
public class Quiz3FindNumInMatrix {
	public static void main(String[] args){
		int[][] arr = new int[][]{
				{1,2,8,9},
				{2,4,9,12},
				{4,7,10,13},
				{6,8,11,15}
		};

		System.out.println(findTarget(arr, 100));
	}

	static boolean findTarget(int[][] arr, int target){
		if (arr.length == 0 || arr[0].length == 0){
			return false;
		}

		int left = 0, down = arr.length-1;

		while (left != arr[0].length && down != 0){
			int cur = arr[down][left];
			if (target == cur){
				return true;
			}else if (target > cur){
				left++;
			}else {
				down--;
			}
		}

		return false;
	}
}
