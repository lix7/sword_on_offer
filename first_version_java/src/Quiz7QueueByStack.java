import java.util.ArrayList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/16
 */
public class Quiz7QueueByStack {
	public static void main(String[] args) {
		Queue q = new Queue();
		q.push(1);
		q.push(2);
		q.push(3);
		q.push(4);

		while (!q.isEmpty()) {
			System.out.println(q.out());
		}
	}
}


class Queue {
	// note 注意这个实现，本质上是一个用于按队列顺序存储数据，一个按照栈顺序存储顺序，需要出队列的时候检查队列栈数据，如果没有就把入栈的数据倒腾到队列栈中再出队列


	List<Integer> insertStack = new ArrayList<>();
	List<Integer> orderedStack = new ArrayList<>();

	public void push(int val) {
		insertStack.add(val);
	}

	public int out() {
		if (orderedStack.isEmpty()) {
			while (!insertStack.isEmpty()){
				orderedStack.add(insertStack.get(insertStack.size()-1));
				insertStack.remove(insertStack.size()-1);
			}
		}

		int rtn = orderedStack.get(orderedStack.size()-1);
		orderedStack.remove(orderedStack.size()-1);
		return rtn;
	}

	public boolean isEmpty() {
		return orderedStack.isEmpty() && insertStack.isEmpty();
	}

}
