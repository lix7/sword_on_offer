import datastructure.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lix7 on 2019/2/16
 */
public class Quiz6ReconstructBintree {
	public static void main(String[] args) {
		List<Integer> pre = new ArrayList<>(Arrays.asList(1, 2, 4, 7, 3, 5, 6, 8));
		List<Integer> mid = new ArrayList<>(Arrays.asList(4, 7, 2, 1, 5, 3, 8, 6));

		TreeNode.printByLayer(reconstructBinTree(pre, mid));
	}


	public static TreeNode reconstructBinTree(List<Integer> pre, List<Integer> mid) {
		/* note 需要注意两点：
		 *  1. 前序每次要先找出有效的根节点（也就是每个子树的根节点），第一个存在于中序遍历的节点
		 *  2. 重建左右子树时对于根节点索引的界限判断，不要导致最后的subList异常
		*/
		if (mid.isEmpty()) {
			return null;
		}

		int rootIndex = 0, rootInMidIndex = 0;
		while ( rootIndex < pre.size() && (rootInMidIndex = mid.indexOf(pre.get(rootIndex))) == -1){
			++rootIndex;

			if (rootIndex == pre.size()){
				return null;
			}
		}
 		TreeNode root = new TreeNode().setVal(pre.get(rootIndex));

		if (rootInMidIndex >= 0) {
			root.setLeft(reconstructBinTree(pre.subList(rootIndex + 1, pre.size()), mid.subList(0, rootInMidIndex)));
		}
		if (rootInMidIndex+1 < mid.size()) {
			root.setRight(reconstructBinTree(pre.subList(rootIndex + 1, pre.size()), mid.subList(rootInMidIndex + 1, mid.size())));
		}
		return root;
	}

}
