/**
 * Created by lix7 on 2019/3/11
 */
public class Quiz38NumberOfK {

	public static void main(String[] args){
		System.out.println(numberOfK(new int[]{}, 0));                                      // -1
		System.out.println(numberOfK(new int[]{1,1,2,2,2,2,3,4,5,5,5,5,9,9,9,9,9,9}, 0));   // -1
		System.out.println(numberOfK(new int[]{1,1,2,2,2,2,3,4,5,5,5,5,9,9,9,9,9,9}, 1));   // 2
		System.out.println(numberOfK(new int[]{1,1,2,2,2,2,3,4,5,5,5,5,9,9,9,9,9,9}, 9));   // 6
		System.out.println(numberOfK(new int[]{1,1,2,2,2,2,3,4,5,5,5,5,9,9,9,9,9,9}, 3));   // 1
		System.out.println(numberOfK(new int[]{1,1,2,2,2,2,3,4,5,5,5,5,9,9,9,9,9,9}, 5));   // 4
	}

	public static int numberOfK(int[] arr, int k){
		// 两次二分查找分别查找左边界和右边界

		if (arr.length == 0){
			return -1;
		}

		int leftIndex = -1;
		int i = 0, j = arr.length-1;
		while (i <= j) {
			int mid = i + (j - i) / 2;

			if (arr[mid] == k) {
				if (arr[i] == k) {
					leftIndex = i;
					break;
				}else {
					j = mid;
					++i;
				}
			}else if (arr[mid] > k) {
				j = mid-1;
			}else {
				i = mid+1;
			}
		}

		int rightIndex = -1;
		i = 0;
		j = arr.length-1;
		while (i <= j) {
			int mid = i + (j - i) / 2;

			if (arr[mid] == k) {
				if (arr[j] == k) {
					rightIndex = j;
					break;
				}else {
					i = mid;
					--j;
				}
			}else if (arr[mid] > k) {
				j = mid-1;
			}else {
				i = mid+1;
			}
		}

		if (leftIndex == -1 || rightIndex == -1){
			return -1;
		}else {
			return rightIndex - leftIndex + 1;
		}
	}

}
