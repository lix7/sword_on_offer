/**
 * Created by lix7 on 2019/3/2
 */
public class Quiz31MaxContinuousSubarraySum {
	public static void main(String[] args){
		System.out.println(calcMaxSum(new int[]{}));
		System.out.println(calcMaxSum(new int[]{-1,-2,-3}));
		System.out.println(calcMaxSum(new int[]{4,-1,-1,-1,2,0,0,1,1,-5}));
		System.out.println(calcMaxSum(new int[]{4,-1,-1,-1,2,0,0,1,1,-5,2,3,6}));
		System.out.println(calcMaxSum(new int[]{4,-1,-1,-1,2,0,0,1,1,-5,100,2}));
	}


	public static int calcMaxSum(int[] arr){

		// note
		//  自己经常不注意的两点：
		//  1. 数组中全是负数的情况
		//  2. 最后一步检查当前的sum

		if (arr.length == 0){
			return 0;
		}

		int maxSum = Integer.MIN_VALUE, curSum = Integer.MIN_VALUE;

		for (int i = 0; i < arr.length; ++i){
			int cur = arr[i];

			if (curSum+cur <= 0){
				maxSum = curSum > maxSum ? curSum : maxSum;
				curSum = Integer.MIN_VALUE;
			}else {
				if (curSum == Integer.MIN_VALUE) {
					curSum = cur;
				}else {
					curSum += cur;
				}
			}
		}
		// 自己经常忘了最后这一步
		maxSum = curSum > maxSum ? curSum : maxSum;

		return maxSum;
	}

}
