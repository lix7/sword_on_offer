/**
 * Created by lix7 on 2019/2/17
 */
public class Quiz10CountOneOfBits {
	public static void main(String[] args){
		System.out.println(countOne(1));
		System.out.println(countOne(2));
		System.out.println(countOne(3));
		System.out.println(countOne(-3));

		System.out.println(countOneQuickly(1));
		System.out.println(countOneQuickly(2));
		System.out.println(countOneQuickly(3));
		System.out.println(countOneQuickly(5));


	}

	public static int countOne(int num){
		/* note
		 *  注意java中存储负数是补码。
		 *  例如-1
		 *  原码1001
		 *  反码1110 对于负数：符号位不变，其它位取反；对于正数：反码等于原码
		 *  补码1111 对于负数：反码+1；对于正数：补码等于原码
		 */
		int count = 0;
		int mask = 1;
		while (mask > 0){
			count += (num & mask) > 0 ? 1 : 0;
			mask <<= 1;
		}
		return count;
	}

	public static int countOneQuickly(int num){
		/* note 这是一个特殊的有新意的解法，记住！
		 *  其意义是无论对于任何正数还是负数，num & (num-1)都代表着消去这个数字的最后一个1，
		 *  这样不停循环就可以将数字归零并且只需要进行n次这样的操作，n为数中1的个数
		 */


		int count = 0;
		while (num > 0){
			count += 1;
			num = num & (num-1);
		}
		return count;
	}
}
