import datastructure.ListNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/3/8
 */
public class Quiz37FirstCommonNodesInLists {
	public static void main(String[] args){
		ListNode head1 = ListNode.fromList(Arrays.asList(1,2,3,4,5,6,7));
		System.out.println(findFirstCommonNodesInList(head1, head1.getNext().getNext().getNext()));


		ListNode head2 = ListNode.fromList(Arrays.asList(101,102));
		head2.getNext().setNext(head1);
		System.out.println(findFirstCommonNodesInList(head1, head2));

		System.out.println(findFirstCommonNodesInList(head1, null));

		System.out.println(findFirstCommonNodesInList(null, null));
	}


	static ListNode findFirstCommonNodesInList(ListNode head1, ListNode head2){
		int head1Length = calcLength(head1);
		int head2length = calcLength(head2);
		int diff = Math.abs(head1Length-head2length);

		ListNode longList = head1, shortList = head2;
		if (head1Length < head2length){
			longList = head2;
			shortList = head1;
		}

		while (diff-- != 0){
			longList = longList.getNext();
		}

		while (longList != shortList){
			longList = longList.getNext();
			shortList = shortList.getNext();
		}

		return longList;
	}

	static int calcLength(ListNode head){
		int rtn = 0;
		while (head != null){
			++rtn;
			head = head.getNext();
		}
		return rtn;
	}

}
