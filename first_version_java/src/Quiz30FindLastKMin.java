import datastructure.MaxHeap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lix7 on 2019/3/1
 */
public class Quiz30FindLastKMin {

	public static void main(String[] args) throws Exception {
		System.out.println(findLastKMinByPartialSort(new int[]{5,2,4,6,8,3,1,}, 0));
		System.out.println(findLastKMinByPartialSort(new int[]{5,2,4,6,8,3,1,}, 1));
		System.out.println(findLastKMinByPartialSort(new int[]{5,2,4,6,8,3,1,}, 3));
		System.out.println(findLastKMinByPartialSort(new int[]{5,2,4,6,8,3,1,}, 100));

		System.out.println("------------------------");

		System.out.println(findLastKMinByHeapSort(new int[]{5,2,4,6,8,3,1,}, 0));
		System.out.println(findLastKMinByHeapSort(new int[]{5,2,4,6,8,3,1,}, 1));
		System.out.println(findLastKMinByHeapSort(new int[]{5,2,4,6,8,3,1,}, 3));
		System.out.println(findLastKMinByHeapSort(new int[]{5,2,4,6,8,3,1,}, 100));
	}

	static List<Integer> findLastKMinByPartialSort(int[] arr, int k){
		if (k >= arr.length){
			return Arrays.stream(arr).boxed().collect(Collectors.toList());
		}

		partialSort(arr, 0, arr.length-1, k);
		return Arrays.stream(arr).boxed().collect(Collectors.toList()).subList(0, k);
	}

	private static int partialSort(int[] arr, int left, int right, int k){
		// 快排的思想

		if (left >= right){
			return left;
		}

		int i = left, j = right;
		int key = arr[left];
		while (true){
			while (i < right && arr[i] <= key){
				++i;
			}

			while (j > left && arr[j] >= key){
				--j;
			}

			if (i >= j){
				break;
			}

			int tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}

		arr[left] = arr[j];
		arr[j] = key;

		if (j == k-1){
			return j;
		}else if (j > k-1){
			return partialSort(arr, left, j-1, k);
		}else {
			return partialSort(arr, j+1, right, k);
		}

	}


	static List<Integer> findLastKMinByHeapSort(int[] arr, int k) throws Exception {
		// note 重要！注意这里是反过来的，如果需要记录最小的K个数，那么需要维持一个容量为K的最大堆保存最小K个数，当新元素小于堆顶元素时，使用当前值替换堆顶值并调整堆;
		//  控制堆的大小应该是外界调用的任务
		if (k <= 0){
			return new ArrayList<>();
		}

		MaxHeap maxHeap = new MaxHeap(k);
		for (int num: arr){
			if (maxHeap.size() < maxHeap.getCapacity()) {
				maxHeap.add(num);
			}else {
				if (num < maxHeap.topVal()){
					maxHeap.getTop();
					maxHeap.add(num);
				}
			}
		}

		List<Integer> rtnList = new ArrayList<>();
		while (!maxHeap.isEmpty()){
			rtnList.add(maxHeap.getTop());
		}

		return rtnList;
	}
}
