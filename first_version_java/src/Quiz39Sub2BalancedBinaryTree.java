import datastructure.TreeNode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lix7 on 2019/3/11
 */
public class Quiz39Sub2BalancedBinaryTree {
	public static void main(String[] args){
		TreeNode root = TreeNode.fromList(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
		System.out.println(isBalancedBinaryTree(root));

		root = TreeNode.fromList(Arrays.asList(1,2,null,3,null,null,null,4));
		System.out.println(isBalancedBinaryTree(root));
	}



	public static boolean isBalancedBinaryTree(TreeNode root){
		return isBalancedBinaryTreeImpl(root, new HashMap<>(), 0);
	}

	private static boolean isBalancedBinaryTreeImpl(TreeNode root, Map<TreeNode, Integer> depthMap, int curDepth){
		// 用一个map缓存了深度信息

		if (root == null){
			return true;
		}

		int leftDepth = getDepth(root.getLeft(), depthMap, curDepth);
		int rightDepth = getDepth(root.getRight(), depthMap, curDepth);

		if (Math.abs(leftDepth-rightDepth) <= 1){
			return isBalancedBinaryTreeImpl(root.getLeft(), depthMap, curDepth+1) && isBalancedBinaryTreeImpl(root.getRight(), depthMap, curDepth+1);
		}else {
			return false;
		}
	}

	private static int getDepth(TreeNode root, Map<TreeNode, Integer> depths, int curDepth){
		if (root == null){
			return curDepth;
		}

		if (depths.containsKey(root)){
			return depths.get(root);
		}else {
			int depth = Math.max(getDepth(root.getLeft(), depths, curDepth+1),
								getDepth(root.getRight(), depths, curDepth+1)
			);

			depths.put(root, depth);
			return depth;
		}
	}

}
