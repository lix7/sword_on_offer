import datastructure.ListNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz16ReverseLinkedList {

	public static void main(String[] args){
		reverseLinkedList(ListNode.fromList(Arrays.asList(1))).print();
		reverseLinkedList(ListNode.fromList(Arrays.asList(1,2))).print();
		reverseLinkedList(ListNode.fromList(Arrays.asList(1,2,3,4,5))).print();
	}

	public static ListNode reverseLinkedList(ListNode head){
		if (head == null || head.getNext() == null){
			return head;
		}

		ListNode pre = null, cur = head, next = head.getNext();

		while (cur != null){
			next = cur.getNext();
			cur.setNext(pre);
			pre = cur;
			cur = next;
		}

		return pre;
	}


}
