import util.Utils;

/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz14OddFirst {
	public static void main(String[] args){
		int[] arr = new int[]{1,2,3,4,5,6,7,8};
		oddFirst(arr);
		Utils.printArray(arr);

		arr = new int[]{1,3,5,2,4,6};
		oddFirst(arr);
		Utils.printArray(arr);

		arr = new int[]{1,3,5};
		oddFirst(arr);
		Utils.printArray(arr);
	}


	public static void oddFirst(int[] arr){
		/* note 调整数组使奇数位于偶数前面
		 *  本质是快排的partition思想
		 */

		int left = 0, right = arr.length-1;

		while (true){

			while (left < arr.length && (arr[left]%2 == 1)){
				++left;
			}

			while (right > 0 && (arr[right]%2 == 0)){
				--right;
			}

			if (left >= right){
				break;
			}

			int tmp = arr[left];
			arr[left] = arr[right];
			arr[right] = tmp;
		}
	}

}
