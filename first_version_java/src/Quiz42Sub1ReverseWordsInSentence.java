import util.Utils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by lix7 on 2019/3/21
 */
public class Quiz42Sub1ReverseWordsInSentence {
	public static void main(String[] args){
		System.out.println(String.valueOf(reverse("i'm the world!".toCharArray())));
		System.out.println(String.valueOf(reverse("i'm the  world!".toCharArray())));
	}

	public static char[] reverse(char[] sentence){
		// note 书上的解法
		reversePartial(sentence, 0, sentence.length-1);

		int i = 0, j = 0;
		while (j < sentence.length){
			char curChar = sentence[j];
			if (curChar == ' '){
				reversePartial(sentence, i, j-1);
				++j;
				i = j;
			}else {
				++j;
			}
		}
		reversePartial(sentence, i, j-1);
		return sentence;
	}

	private static void reversePartial(char[] arr, int src, int dst){

		for (int i = 0; i <= (dst-src)/2; ++i){
			Utils.swapInArr(arr, src+i, dst-i);
		}

	}

	public static char[] reverseByLix(char[] sentence){
		// 我自己的辣鸡写法，太烂了，感觉自己的思想已经被禁锢了...

		char[] rtn = new char[sentence.length];
		Arrays.fill(rtn, ' ');

		int oi = 0, oj = 0;
		int ri = sentence.length;

		while (oj != sentence.length){

			char curChar = sentence[oj];

			if (curChar == ' '){
				System.arraycopy(sentence, oi, rtn, ri, oj-oi);
				oi = oj;
				while (oi < sentence.length && sentence[oi] == ' '){
					++oi;
					--ri;
				}
				oj = oi;
			}else {
				++oj;
				--ri;
			}
		}

		System.arraycopy(sentence, oi, rtn, ri, oj-oi);
		return rtn;
	}


}
