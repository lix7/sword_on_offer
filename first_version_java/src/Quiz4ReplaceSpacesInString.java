/**
 * Created by lix7 on 2019/2/13
 */
public class Quiz4ReplaceSpacesInString {
	public static void main(String[] args) {
		String sentence = "we%20are%20the%20world!%2";
		System.out.println(trimN2(sentence.toCharArray()));
		System.out.println(trimN(sentence.toCharArray(), "%20".toCharArray()));
	}

	static char[] trimN2(char[] arr) {
		char[] space = "%20".toCharArray();

		OUT:
		for (int i = 0; i < arr.length; ++i) {
			if (arr[i] == space[0]) {
				for (int j = 0; j < space.length; ++j) {
					if (i + j >= arr.length || arr[i + j] != space[j]) {
						continue OUT;
					}
				}

				arr[i] = ' ';
				for (int k = i + 1; k < arr.length - (space.length - 1); ++k) {
					arr[k] = arr[k + space.length - 1];
				}
			}
		}

		return arr;
	}

	static char[] trimN(char[] arr, char[] trim){
		// note 这个方法要记住，双指针

		if (trim.length == 0){
			return arr;
		}

		int oldOffset = 0;

		for (int newIndex = 0; newIndex+oldOffset < arr.length; ++newIndex){
			LOOP:
			if (arr[newIndex+oldOffset] == trim[0]){
				for (int i = 0; i < trim.length; ++i){
					if (newIndex+oldOffset+i >= arr.length || arr[newIndex+oldOffset+i] != trim[i]){
						break LOOP;
					}

				}

				System.out.println(arr);
				oldOffset += trim.length-1;
				arr[newIndex+oldOffset] = ' ';
			}

			arr[newIndex] = arr[newIndex+oldOffset];
		}


		return arr;
	}
}
