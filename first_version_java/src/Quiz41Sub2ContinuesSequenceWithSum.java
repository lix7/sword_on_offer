import util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lix7 on 2019/3/21
 */
public class Quiz41Sub2ContinuesSequenceWithSum {
	public static void main(String[] args){
		System.out.println(seqWithSum(-1));
		System.out.println(seqWithSum(1));
		System.out.println(seqWithSum(14));
		System.out.println(seqWithSum(15));
		System.out.println(seqWithSum(100));
	}


	public static List<List<Integer>> seqWithSum(int sum){
		int lo = 1, hi = 2;
		List<List<Integer>> rtn = new ArrayList<>();

		while (true){
			if (lo >= (sum+1)/2){
				return rtn;
			}

			int curSum = (lo + hi) * (hi-lo+1) / 2;
			if (curSum == sum){
				rtn.add(Utils.toIntegerList(new int[]{lo, hi}));
				++lo;
			}else if (curSum < sum){
				++hi;
			}else{
				++lo;
			}
		}
	}

}
