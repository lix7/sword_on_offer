/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz11QuickPow {
	public static void main(String[] args){
		System.out.println(pow(0,2));
		System.out.println(pow(1,4));
		System.out.println(pow(2,3));
		System.out.println(pow(2,4));
		System.out.println(pow(2,5));
		System.out.println(pow(2,-3));

	}

	public static double pow(double base, int e){
		// note 这个方法比较迭代乘base e次要快一些；除此以外，注意0底数、0幂、负数幂的异常处理

		if (base == 0 || base == 1 || e == 0){
			return 1;
		}

		boolean mFlag = e < 0;

		if (mFlag){
			e = -e;
		}

		int count = 1;
		double rtn = base;
		while (count*2 < e){
			rtn *= rtn;
			count <<= 1;
		}

		for (;count < e; ++count){
			rtn *= base;
		}

		if (mFlag){
			rtn = 1/rtn;
		}

		return rtn;
	}
}
