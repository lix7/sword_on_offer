import javax.swing.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by lix7 on 2019/3/30
 */
public class Quiz43DicesProbability {

	// note 个人感觉这题和数学建模没什么关系，更多的是对DP的理解，和递归、循环两种实现的性能分析

	public static void main(String[] args){
		System.out.println(calcProbRecursively(2));
		System.out.println(calcProbRecurrently(2));
	}

	public static Map<Integer, Double> calcProbRecursively(int n){
		Map<Integer, Integer> res = new HashMap<>();
		for (int i = n; i <= 6*n; ++i){
			Map<Integer, Integer> curRes = calcProbRecursivelyImpl(n, i);

			for (int key: curRes.keySet()){
				if (res.containsKey(key)){
					res.put(key, res.get(key)+curRes.get(key));
				}else {
					res.put(key, curRes.get(key));
				}
			}
		}

		Map<Integer, Double> rtn = new HashMap<>();
		res.forEach((k,v)->{
			rtn.put(k, (double)v);
		});
		double allCnt = rtn.values().stream().mapToDouble(Double::doubleValue).sum();

		return rtn.entrySet().stream().map((e)->{
			e.setValue(e.getValue()/allCnt);
			return e;
		}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}

	private static Map<Integer, Integer> calcProbRecursivelyImpl(int n, int s){
		if (n <= 0 || s <= 0 || s > 6*n){
			return new HashMap<>();
		}
		if (n == 1){
			HashMap<Integer, Integer> rtn = new HashMap<>();
			for (int i = 1; i <= 6; ++i){
				rtn.put(i, 1);
			}
			return rtn;
		}

		Map<Integer, Integer> res = new HashMap<>();
		for (int i = 1; i <= 6; ++i){
			Map<Integer, Integer> curRes = calcProbRecursivelyImpl(n-1, s-i);

			for (int subKey: curRes.keySet()){
				int key = subKey+i;
				if (res.containsKey(key)){
					res.put(key, res.get(key)+curRes.get(subKey));
				}else {
					res.put(key, curRes.get(subKey));
				}
			}
		}

		return res;

	}


	public static Map<Integer, Double> calcProbRecurrently(int n){
		if (n <= 0){
			return new HashMap<>();
		}

		List<Map<Integer, Integer>> recordList = new ArrayList<>();
		recordList.add(new HashMap<>());
		recordList.add(new HashMap<>());
		for (int i = 1; i <= 6; ++i){
			recordList.get(1).put(i, 1);
		}

		for (int i = 2; i <= n; ++i){
			Map<Integer, Integer> curRecord = new HashMap<>();
			Map<Integer, Integer> preRecord = recordList.get(i-1);

			for (int j = 1; j <= 6; ++j){
				for (int k: preRecord.keySet()){
					if (curRecord.containsKey(k+j)){
						curRecord.put(k+j, curRecord.get(k+j)+1);
					}else {
						curRecord.put(k+j, preRecord.get(k));
					}
				}
			}

			recordList.add(curRecord);
		}

		Map<Integer, Integer> res = recordList.get(n);

		Map<Integer, Double> rtn = new HashMap<>();
		res.forEach((k,v)->{
			rtn.put(k, (double)v);
		});
		double allCnt = rtn.values().stream().mapToDouble(Double::doubleValue).sum();

		return rtn.entrySet().stream().map((e)->{
			e.setValue(e.getValue()/allCnt);
			return e;
		}).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	}
}
