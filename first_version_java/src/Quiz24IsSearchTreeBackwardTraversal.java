import java.util.Arrays;
import java.util.List;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz24IsSearchTreeBackwardTraversal {
	public static void main(String[] args){
		System.out.println(isBackwardTraversal(Arrays.asList(2,6,5,15,25,10)));
		System.out.println(isBackwardTraversal(Arrays.asList(2,6,5,15,25,16)));
		System.out.println(isBackwardTraversal(Arrays.asList(1,2,10)));
		System.out.println(isBackwardTraversal(Arrays.asList(1,2,15,9,10)));
	}

	public static boolean isBackwardTraversal(List<Integer> list){
		// note 分治，搜索树左子树一定都小于根节点，右子树一定都大于根节点；Sublist不如直接参数限制范围好用；这道题每次遍历的上限其实是list.size()-2，因为最后一个值时root的值，不应该参加比较。

		if (list.isEmpty()){
			return true;
		}

		int root = list.get(list.size() - 1);

		int partitionIndex = 0;
		while (partitionIndex < list.size()-1){
			if (list.get(partitionIndex) > root){
				break;
			}

			++partitionIndex;
		}

		int j = partitionIndex;
		while (j < list.size()-1){
			if (list.get(j) < root){
				return false;
			}

			++j;
		}

		boolean left = true, right = true;
		if (partitionIndex < list.size()){
			right = isBackwardTraversal(list.subList(partitionIndex, list.size()-1));
		}
		return isBackwardTraversal(list.subList(0, partitionIndex)) && right;
	}
}
