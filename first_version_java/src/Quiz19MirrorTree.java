import datastructure.TreeNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/2/22
 */
public class Quiz19MirrorTree {
	public static void main(String[] args){
		TreeNode root = TreeNode.fromList(Arrays.asList(1,2,3,4,5,6,7));
		TreeNode.printByLayer(root);
		mirror(root);
		TreeNode.printByLayer(root);
	}

	public static void mirror(TreeNode root){
		if (root == null || root.getLeft() == null && root.getRight() == null){
			return;
		}

		TreeNode tmp = root.getLeft();
		root.setLeft(root.getRight());
		root.setRight(tmp);

		mirror(root.getLeft());
		mirror(root.getRight());
	}

}
