import util.Utils;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by lix7 on 2019/2/17
 */
public class Quiz8MinNumberInRotatedArray {
	public static void main(String[] args){
		System.out.println(findMin(new int[]{1,2,3,4,5}));
		System.out.println(findMin(new int[]{3,4,5,1,2}));
		System.out.println(findMin(new int[]{4,5,1,2,3}));
		System.out.println(findMin(new int[]{5,1,2,3,4}));

		System.out.println(findMin(new int[]{0,1,1,1,1}));
		System.out.println(findMin(new int[]{1,1,1,0,1}));
		System.out.println(findMin(new int[]{1,0,1,1,1}));
	}


	public static int findMin(int[] arr){
		/* note
		 *  二分查找一类的题目需要注意的就是每次对于mid要加1，避免(0+1)/2=0这种死循环出现
		 *  对于这道题"循环数组中查最小值"，需要注意的是非严格递增情况下，如果遇到left/mid/right对应的值相等的时候，
		 *  我们并没有办法判断最小值在左半部分还是右半部分，此时需要转化为遍历。
		 */

		int i = 0, j = arr.length-1;

		while (true){
			if (arr[i] < arr[j]){
				return arr[i];
			}

			int mid = (i+j)/2;

			// 注意这儿三者相等无法浦安端的情况
			if (arr[i] == arr[j] && arr[j] == arr[mid]){
				return Utils.toIntegerList(arr).subList(i, j+1).stream().min(Integer::compare).get();
			}

			if (arr[mid] >= arr[i]){
				i = mid+1;
			}else {
				j = mid;
			}
		}
	}
}
