import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz22CheckStackSeq {
	public static void main(String[] args){
		System.out.println(check(new LinkedList<>(Arrays.asList(1,2,3,4,5)), new LinkedList<>(Arrays.asList(1,2,3,4,5))));
		System.out.println(check(new LinkedList<>(Arrays.asList(1,2,3,4,5)), new LinkedList<>(Arrays.asList(5,4,3,2,1))));
		System.out.println(check(new LinkedList<>(Arrays.asList(1,2,3,4,5)), new LinkedList<>(Arrays.asList(4,3,5,2,1))));
		System.out.println(check(new LinkedList<>(Arrays.asList(1,2,3,4,5)), new LinkedList<>(Arrays.asList(4,3,5,1,2))));
	}

	public static <T> boolean check(LinkedList<T> pushSeq, LinkedList<T> pollSeq){

		pushSeq = new LinkedList<>(pushSeq);
		pollSeq = new LinkedList<>(pollSeq);
		Collections.reverse(pollSeq);

		LinkedList<T> s = new LinkedList<>();

		while (!pushSeq.isEmpty()){
			s.add(pushSeq.pollFirst());

			while (!s.isEmpty() && s.getLast().equals(pollSeq.getLast())){
				s.pollLast();
				pollSeq.pollLast();
			}
		}

		return s.isEmpty();
	}

}


