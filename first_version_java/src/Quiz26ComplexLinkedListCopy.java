import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz26ComplexLinkedListCopy {
	public static void main(String[] args){
		RandomNextListNode list = RandomNextListNode.fromList(Arrays.asList(1,2,3,4,5));

		RandomNextListNode iter = list;
		while (iter.getNext() != null){
			iter.setRandomNext(iter.getNext().getNext());
			iter = iter.getNext();
		}
		iter.setRandomNext(list);

		System.out.println("------------ Old -----------");
		iter = list;
		while (iter != null){
			System.out.println("Val: " + iter.getVal() + ", RandomNextVal: " + (iter.getRandomNext() != null ? (iter.getRandomNext().getVal() + " " + iter.getRandomNext().toString()) : "N"));
			iter = iter.getNext();
		}

		System.out.println("------------ New -----------");
//		iter = deepCopyUsingExtraSpace(list);
		iter = deepCopy(list);
		while (iter != null){
			System.out.println("Val: " + iter.getVal() + ", RandomNextVal: " + (iter.getRandomNext() != null ? (iter.getRandomNext().getVal() + " " + iter.getRandomNext().toString()) : "N"));
			iter = iter.getNext();
		}

	}

	public static RandomNextListNode deepCopy(RandomNextListNode origin){
		// note 这个方法自己第一次做完全没想到，记下来！O(n)时间，O(1)空间

		if (origin == null){
			return null;
		}

		RandomNextListNode iter = origin;

		while (iter != null){
			iter.setNext(new RandomNextListNode().setVal(iter.getVal()).setNext(iter.getNext()));
			iter = iter.getNext().getNext();
		}

		iter = origin;
		while (iter != null){
			iter.getNext().setRandomNext(iter.getRandomNext() == null ? null : iter.getRandomNext().getNext());
			iter = iter.getNext().getNext();
		}

		RandomNextListNode rtn = origin.getNext();
		iter = rtn;
		while (iter.getNext() != null){
			iter.setNext(iter.getNext().getNext());
			iter = iter.getNext();
		}

		return rtn;
	}

	public static RandomNextListNode deepCopyUsingExtraSpace(RandomNextListNode origin){
		// O(n)空间复杂度的解法

		Map<RandomNextListNode, RandomNextListNode> map = new HashMap<>();

		RandomNextListNode old = origin;
		RandomNextListNode rtn = new RandomNextListNode().setNext(null);

		RandomNextListNode rtnIter = rtn;
		while (old != null){
			rtnIter.setNext(new RandomNextListNode().setVal(old.getVal()));
			map.put(old, rtnIter.getNext());

			rtnIter = rtnIter.getNext();
			old = old.getNext();
		}
		rtn = rtn.getNext();

		old = origin;
		rtnIter = rtn;
		while (old != null){
			rtnIter.setRandomNext(map.get(old.getRandomNext()));

			old = old.getNext();
			rtnIter = rtnIter.getNext();
		}

		return rtn;
	}


}

class RandomNextListNode {
	private Object val;
	private RandomNextListNode next;
	private RandomNextListNode randomNext;

	public static RandomNextListNode fromList(List<?> list){
		if (list == null || list.isEmpty()){
			return null;
		}

		if (list.size() == 1){
			return new RandomNextListNode().setVal(list.get(0)).setNext(null);
		}

		RandomNextListNode head = new RandomNextListNode();
		RandomNextListNode cur = head;
		int index = -1;
		for (Object element: list){
			++index;

			if (index == 0){
				head.setVal(element).setNext(new RandomNextListNode());
				cur = head.next;
			}else if (index == list.size()-1){
				cur.setVal(element).setNext(null);
			}else {
				cur.setVal(element).setNext(new RandomNextListNode());
				cur = cur.next;
			}
		}

		return head;
	}

	public void print() {
		RandomNextListNode cur = this;
		while(cur != null){
			System.out.print(cur.val + " ");
			cur = cur.next;
		}
		System.out.println("");
	}

	public RandomNextListNode get(int n){
		RandomNextListNode cur = this;
		for (int i = 0; i < n; ++i){
			cur = cur.getNext();
		}
		return cur;
	}

	public Object getVal() {
		return val;
	}

	public RandomNextListNode setVal(Object val) {
		this.val = val;
		return this;
	}

	public RandomNextListNode getNext() {
		return next;
	}

	public RandomNextListNode setNext(RandomNextListNode next) {
		this.next = next;
		return this;
	}

	public RandomNextListNode getRandomNext() {
		return randomNext;
	}

	public RandomNextListNode setRandomNext(RandomNextListNode randomNext) {
		this.randomNext = randomNext;
		return this;
	}
}
