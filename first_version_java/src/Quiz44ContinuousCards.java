import java.util.Arrays;
import java.util.List;

/**
 * Created by lix7 on 2019/3/30
 */
public class Quiz44ContinuousCards {
	public static void main(String[] args){
		System.out.println(isContinuousCards(Arrays.asList(1,2,3,4,5)));    // true
		System.out.println(isContinuousCards(Arrays.asList(1,2,3,4,6)));    // false
		System.out.println(isContinuousCards(Arrays.asList(1,3,5,0,0)));    // true
		System.out.println(isContinuousCards(Arrays.asList(0,0,1,2,5)));    // true
		System.out.println(isContinuousCards(Arrays.asList(0,1,2,3,5)));    // true
		System.out.println(isContinuousCards(Arrays.asList(0,1,2,3,10)));   // false
	}


	public static boolean isContinuousCards(List<Integer> cards){
		// note 思路很简单，但细节比较多，写代码注意考虑清除
		// cards数值取值范围0-13，0代表大小王，可以替代任何数字

		cards.sort(Integer::compareTo);

		int jokerCnt = 0;
		for (Integer card : cards) {
			if (card == 0) {
				++jokerCnt;
			}else {
				break;
			}
		}

		int prevCard = -1;

		for (int card: cards){
			if (card == 0){
				continue;
			}

			if (prevCard == -1){
				prevCard = card;
				continue;
			}

			if (card == prevCard){
				return false;
			}

			if (card - prevCard == 1){
			}else if (jokerCnt >= card - prevCard - 1){
				jokerCnt = jokerCnt - (card - prevCard - 1);
			}else {
				return false;
			}

			prevCard = card;
		}

		return true;
	}

}
