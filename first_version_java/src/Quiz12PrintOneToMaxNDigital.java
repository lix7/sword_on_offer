import java.util.ArrayList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz12PrintOneToMaxNDigital {
	public static void main(String[] args){
		System.out.println("Recursive method");
		printByRecursive(2);

		System.out.println("String method");
		printByString(2);
	}


	public static void printByString(int n){
		// 模拟大数加法的方法
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < n; ++i){
			list.add(0);
		}
		// 通过溢出可以以O(1)效率判断是否达到最大数
		while (!addOne(list)){
			printListNumber(list);
		}
	}

	/**
	 * @param list
	 * @return 是否溢出
	 */
	private static boolean addOne(List<Integer> list){
		for (int digital = list.size()-1; digital >= 0; --digital){
			if (list.get(digital) < 9){
				list.set(digital, list.get(digital)+1);
				return false;
			}

			list.set(digital, 0);
		}

		return true;
	}


	public static void printByRecursive(int n){
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < n; ++i){
			list.add(0);
		}
		printByRecursiveImpl(list, 0);
	}

	public static void printByRecursiveImpl(List<Integer> list, int curDigital){
		// 全排列方法，写起来简单
		if (curDigital == list.size()){
			printListNumber(list);
			return ;
		}

		for (int i = 0; i <= 9; ++i){
			list.set(curDigital, i);
			printByRecursiveImpl(list, curDigital+1);
		}
	}

	private static void printListNumber(List<Integer> list){
		boolean flag = false;
		for (int num: list){
			if (num != 0){
				flag = true;
			}

			if (flag){
				System.out.print(num);
			}
		}
		System.out.println("");
	}

}
