/**
 * Created by lix7 on 2019/2/22
 */
public class Quiz20ClockPrintMatrix {
	public static void main(String[] args){
		int[][] matrix = new int[][]{
				{ 1, 2, 3, 4},
				{12,13,14, 5},
				{11,16,15, 6},
				{10, 9, 8, 7}
		};

		matrix = new int[][]{
				{1},
				{2},
				{3},
				{4}
		};

		matrix = new int[][]{
				{ 1, 2, 3},
				{12,13,14},
				{11,16,15},
				{10, 9, 8}
		};

		clockPrintMatrix(
			matrix,
			0, matrix.length-1, matrix[0].length-1, 0, 0, 0
		);
	}

	public static void clockPrintMatrix(int[][] matrix, int left, int bottom, int right, int top, int curR, int curC){
		// note 这不是书里给的解法，这个解法不直观，但是写起来很简洁，避免了一般的四个循环的重复代码

		int rInterval = 0, cInterval = 0, rOffset = 0, cOffset = 0;
		int leftOffset = 0, bottomOffset = 0, rightOffset = 0, topOffset = 0;

		if (left > right || top > bottom){
			return;
		}

		if (curR == top && curC == left){
			cInterval = 1;
			topOffset = 1;
			rOffset = 1;
		}
		else if (curR == top && curC == right){
			rInterval = 1;
			rightOffset = -1;
			cOffset = -1;
		}
		else if (curR == bottom && curC == right){
			cInterval = -1;
			bottomOffset = -1;
			rOffset = -1;
		}
		else if (curR == bottom && curC == left){
			rInterval = -1;
			leftOffset = 1;
			cOffset = 1;
		}

		while (curR >= top && curR <= bottom && curC >= left && curC <= right){
			System.out.print(matrix[curR][curC] + " ");

			curR += rInterval;
			curC += cInterval;
		}

		clockPrintMatrix(matrix, left+leftOffset, bottom+bottomOffset, right+rightOffset, top+topOffset, curR-rInterval+rOffset, curC-cInterval+cOffset);
	}

}
