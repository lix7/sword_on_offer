import datastructure.ListNode;
import datastructure.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz13DeleteLinkedListNode {
	public static void main(String[] args){
		ListNode head = ListNode.fromList(new ArrayList<>(Arrays.asList(1,2,3,4,5)));
		ListNode cur = head;
		while (cur.getNext() != null){
			cur = cur.getNext();
		}

		deleteNode(head, cur);
		head.print();
		deleteNode(head, head);
		head.print();
	}

	public static void deleteNode(ListNode head, ListNode toDeletedNode){
		/* note
		 *  比较有意思的方法，通过使用后续节点数据覆盖当前节点数据达到删除效果。
		 *  需要注意的是要删除节点是尾节点的特殊情况，此时只能遍历到尾节点的前一个节点。
		 *  这个方法无法判断要删除节点是否为链表中的节点。
		 */

		if (head == null || toDeletedNode == null){
			return;
		}

		// 尾节点特殊情况
		if (toDeletedNode.getNext() == null){
			ListNode cur = head;
			while (cur.getNext() != toDeletedNode){
				cur = cur.getNext();
			}
			cur.setNext(null);

			return;
		}

		// 非尾节点
		ListNode next = toDeletedNode.getNext();
		toDeletedNode.setVal(next.getVal());
		toDeletedNode.setNext(next.getNext());
	}



}
