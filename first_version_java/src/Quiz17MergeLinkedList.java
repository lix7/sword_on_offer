import datastructure.ListNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/2/22
 */
public class Quiz17MergeLinkedList {
	public static void main(String[] args){
		System.out.println(merge(null, null) == null);
		mergeRecursively(null, ListNode.fromList(Arrays.asList(1))).print();
		mergeRecursively(ListNode.fromList(Arrays.asList(1)), null).print();
		mergeRecursively(ListNode.fromList(Arrays.asList(3)), ListNode.fromList(Arrays.asList(1,2))).print();
		mergeRecursively(ListNode.fromList(Arrays.asList(1,3,5,7,9)), ListNode.fromList(Arrays.asList(2,4,6,8,10))).print();
	}

	public static ListNode mergeRecursively(ListNode head1, ListNode head2){
		// note 递归写法更简洁优雅

		if (head1 == null){
			return head2;
		}

		if (head2 == null){
			return head1;
		}

		ListNode rtn = null;

		if ((int)head1.getVal() <= (int)head2.getVal()){
			rtn = head1;
			rtn.setNext(mergeRecursively(head1.getNext(), head2));
			return rtn;
		}else {
			rtn = head2;
			rtn.setNext(mergeRecursively(head1, head2.getNext()));
			return rtn;
		}
	}


	public static ListNode merge(ListNode head1, ListNode head2){
		// 迭代的写法比较麻烦

		ListNode rtnHead = null, rtnIndex = null;

		if (head1 == null){
			return head2;
		}

		if (head2 == null){
			return head1;
		}

		if ((int)head1.getVal() <= (int)head2.getVal()){
			rtnHead = head1;
			head1 = head1.getNext();
		}else {
			rtnHead = head2;
			head2 = head2.getNext();
		}

		rtnIndex = rtnHead;

		while (head1 !=  null || head2 != null){
			if (head1 == null){
				rtnIndex.setNext(head2);
				return rtnHead;
			}

			if (head2 == null){
				rtnIndex.setNext(head1);
				return rtnHead;
			}

			if ((int)head1.getVal() <= (int)head2.getVal()){
				rtnIndex.setNext(head1);
				head1 = head1.getNext();
			}else {
				rtnIndex.setNext(head2);
				head2 = head2.getNext();
			}
			rtnIndex = rtnIndex.getNext();

		}

		return rtnHead;
	}

}
