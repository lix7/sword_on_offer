import datastructure.ListNode;
import datastructure.TreeNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/2/18
 */
public class Quiz15LastKNode {
	public static void main(String[] args){
		ListNode head = ListNode.fromList(Arrays.asList(1,2,3,4,5));

		System.out.println(findLastKNode(head, 0));
		System.out.println(findLastKNode(head, 1));
		System.out.println(findLastKNode(head, 2));
		System.out.println(findLastKNode(head, 3));
		System.out.println(findLastKNode(head, 4));
		System.out.println(findLastKNode(head, 5));
		System.out.println(findLastKNode(head, 6));
	}

	public static ListNode findLastKNode(ListNode head, int k){
		// note 链表倒数第k个节点：这道题需要注意检查k大于链表长度时的异常情况，并且要注意边界条件时不要NPE

		if (head == null){
			return null;
		}

		ListNode former = head, later = head;

		for (int i = 0; i < k; ++i){
			if (former == null){
				return null;
			}
			former = former.getNext();
		}

		while (true){
			if (former == null){
				return later;
			}

			former = former.getNext();
			later = later.getNext();
		}
	}

}
