/**
 * Created by lix7 on 2019/2/27
 */
public class Quiz29FindHalfNum {

	public static void main(String[] args){
		System.out.println(findHalfNum(new int[]{1,2,3,2,2,2,5,4,2}));
		System.out.println(findHalfNum(new int[]{1,2,3,4,5}));

		System.out.println(findHalfNumByNumericMethod(new int[]{1,2,3,2,2,2,5,4,2}));
		System.out.println(findHalfNumByNumericMethod(new int[]{1,2,3,4,5}));
	}

	public static int findHalfNum(int[] arr){
		int midNum = findMidNum(arr, 0, arr.length-1);
		int count = 0;
		for (int i = 0; i < arr.length; ++i){
			if (arr[i] == midNum){
				++count;
			}
		}
		if (count > arr.length/2){
			return midNum;
		}else {
			return -1;
		}
	}

	private static int findMidNum(int[] arr, int left, int right){
		/* todo note 通过Partition的方法来实现，相当于是不完全的快排
		 *  前提是确定如果一个数字占据了整个数组的一半以上，那他一定是这个数组的中位数
		 */

		if (left >= right){
			return left;
		}

		int i = left, j = right;
		int key = arr[left];

		while (true) {
			while (i < right && arr[i] <= key){
				++i;
			}

			while (j > left && arr[j] >= key){
				--j;
			}

			if (i >= j){
				break;
			}

			int tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;

		}

		arr[left] = arr[j];
		arr[j] = key;

		if (j > arr.length/2){
			return findMidNum(arr, left, j-1);
		}else if (j < arr.length/2){
			return findMidNum(arr, j+1, right);
		}else {
			return key;
		}

	}


	public static int findHalfNumByNumericMethod(int[] arr){
		// note 特别注意这道题这个解法！书上的第二种取巧方法，思路是如果有一个数字过半，一个该数字与一个其他数字相抵，最后总能剩下大于等于一个这个数字

		if (arr.length == 0){
			return -1;
		}

		int count = 0, lastNum = -1;

		for (int i = 0; i < arr.length; ++i){
			if (count == 0){
				lastNum = arr[i];
				++count;
			}else {

				if (lastNum == arr[i]){
					++count;
				}else {
					--count;
				}

			}

		}

		// 检查是否真正为占比一半以上的元素
		count = 0;
		for (int i = 0; i < arr.length; ++i){
			if (arr[i] == lastNum){
				++count;
			}
		}
		if (count > arr.length/2){
			return lastNum;
		}else {
			return -1;
		}
	}

}
