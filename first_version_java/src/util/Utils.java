package util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/17
 */
public class Utils {
	public static List<Integer> toIntegerList(int[] arr){
		ArrayList<Integer> rtn = new ArrayList<>();
		for (int num: arr){
			rtn.add(num);
		}
		return rtn;
	}

	public static void printArray(int[] arr){
		for (Integer num: arr){
			System.out.print(num + " ");
		}
		System.out.println("");
	}

	public static void swapInArr(int[] arr, int a, int b){
		int tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}

	public static void swapInArr(char[] arr, int a, int b){
		char tmp = arr[a];
		arr[a] = arr[b];
		arr[b] = tmp;
	}


	public static <E> void swapInList(List<E> list, int a, int b){
		E o = list.get(a);
		list.set(a, list.get(b));
		list.set(b, o);
	}


}
