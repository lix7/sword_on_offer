package quiz0;

import util.Utils;

/**
 * Created by lix7 on 2019/2/16
 */
public class MergeSort {

	public static void main(String[] args){
		System.out.println(Utils.toIntegerList(mergeSort(new int[]{5,4,3,2,1,6}, 0, 5)));
		System.out.println(Utils.toIntegerList(mergeSort(new int[]{5,4,3,2,1}, 0, 4)));
		System.out.println(Utils.toIntegerList(mergeSort(new int[]{1,2,3,4,5}, 0, 4)));
		System.out.println(Utils.toIntegerList(mergeSort(new int[]{1}, 0, 0)));
		System.out.println(Utils.toIntegerList(mergeSort(new int[]{}, 0, 0)));

	}


	public static int[] mergeSort(int[] arr, int left, int right){
		if (left >= right){
			return arr;
		}

		int mid = left+(right-left)/2;
		mergeSort(arr, left, mid);
		mergeSort(arr, mid+1, right);

		int[] sorted = new int[right-left+1];
		int index = 0;
		int i = left, j = mid+1;
		while (i <= mid && j <=right){
			if (arr[i] < arr[j]){
				sorted[index++] = arr[i++];
			}else {
				sorted[index++] = arr[j++];
			}
		}

		while (i <= mid){
			sorted[index++] = arr[i++];
		}

		while (j <= right){
			sorted[index++] = arr[j++];
		}

		for (i = left; i <= right; ++i){
			arr[i] = sorted[i-left];
		}

		return arr;
	}

}
