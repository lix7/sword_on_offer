package quiz0;

/**
 * Created by lix7 on 2019/2/16
 */
public class Binsearch {

	public static void main(String[] args){
		System.out.println(binsearch(new int[]{1,3,5,7,9}, 1));
		System.out.println(binsearch(new int[]{1,3,5,7,9}, 2));
		System.out.println(binsearch(new int[]{1,3,5,7,9}, 9));
		System.out.println(binsearch(new int[]{1,3,5,7,9}, 10));
	}

	public static int binsearch(int[] arr, int val){

		int left = 0, right = arr.length-1;

		while (left <= right){
			int mid = left + (right - left) / 2;
			if (arr[mid] == val){
				return left;
			}else if (arr[mid] > val){
				right = mid-1;
			}else {
				left = mid+1;
			}
		}

		return -1;
	}

}
