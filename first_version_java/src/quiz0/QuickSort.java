package quiz0;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

/**
 * Created by lix7 on 2019/2/16
 */
public class QuickSort {

	public static void main(String[] args){
		int[] arr = new int[]{5,4,3,2,1};
		quicksort(arr, 0, arr.length-1);
		System.out.println(Arrays.stream(arr).boxed().collect(Collectors.toList()));

		int[] arr2 = new int[]{};
		quicksort(arr2, 0, arr2.length-1);
		System.out.println(Arrays.stream(arr2).boxed().collect(Collectors.toList()));

		int[] arr3 = new int[]{9,4,5,8,7,6,1,2,3};
		quicksort(arr3, 0, arr3.length-1);
		System.out.println(Arrays.stream(arr3).boxed().collect(Collectors.toList()));
	}

	private static void swap(int[] arr, int i, int j){
		int tmp = arr[i];
		arr[i] = arr[j];
		arr[j] = tmp;
	}

	public static void quicksort(int[] arr, int left, int right){
		// note “背”是最靠谱的，需要注意的是：1.循环时等于也是要跳过的，因为要避免在循环交换阶段就把中枢交换走了；2.每次扫描的上下界是right/right而非0/arr.length

		if (left >= right){
			return;
		}

		int i = left, j = right;
		int key = arr[left];
		while (true){
			while (i < right && arr[i] <= key){
				++i;
			}

			while (j > left && arr[j] >= key){
				--j;
			}

			if (i >= j){
				break;
			}

			int tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}

		arr[left] = arr[j];
		arr[j] = key;
		quicksort(arr, left, j-1);
		quicksort(arr, j+1, right);
	}
}
