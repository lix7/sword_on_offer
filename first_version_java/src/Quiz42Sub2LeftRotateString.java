import util.Utils;

/**
 * Created by lix7 on 2019/3/21
 */
public class Quiz42Sub2LeftRotateString {
	public static void main(String[] args){
		System.out.println(String.valueOf(rotate("12345".toCharArray(), -1)));
		System.out.println(String.valueOf(rotate("12345".toCharArray(), 0)));
		System.out.println(String.valueOf(rotate("12345".toCharArray(), 1)));
		System.out.println(String.valueOf(rotate("12345".toCharArray(), 4)));
		System.out.println(String.valueOf(rotate("12345".toCharArray(), 5)));
		System.out.println(String.valueOf(rotate("12345".toCharArray(), 6)));
	}

	public static char[] rotate(char[] arr, int k){
		// note 小米实习的面试题

		if (k >= arr.length){
			k %= arr.length;
		}

		if (k <= 0){
			return arr;
		}

		k = arr.length - k;

		reversePartial(arr, 0, arr.length-1);
		reversePartial(arr, 0, k-1);
		reversePartial(arr, k, arr.length-1);

		return arr;
	}

	private static void reversePartial(char[] arr, int src, int dst){

		for (int i = 0; i <= (dst-src)/2; ++i){
			Utils.swapInArr(arr, src+i, dst-i);
		}

	}

}
