package datastructure;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Created by lix7 on 2019/2/16
 */
public class TreeNode {
	Object val;
	TreeNode left;
	TreeNode right;
	TreeNode parent;

	public static TreeNode fromList(List<Integer> list){
		list = new ArrayList<>(list);
		// 添加占位用的元素，使列表符合树的定义
		list.add(0, null);
		List<TreeNode> treeNodeList = new ArrayList<>(list.size());
		for (int i = 0; i < list.size(); ++i){
			if (list.get(i) != null) {
				treeNodeList.add(new TreeNode().setVal(list.get(i)));
			}else {
				treeNodeList.add(null);
			}
		}

		for (int i = 1; i < list.size(); ++i){
			TreeNode cur = treeNodeList.get(i);

			if (cur == null){
				continue;
			}

			if (2*i < list.size()){
				cur.setLeft(treeNodeList.get(2*i));
			}
			if (2*i+1 < list.size()){
				cur.setRight(treeNodeList.get(2*i+1));
			}

			if (i/2 > 0){
				cur.setParent(treeNodeList.get(i/2));
			}
		}

		return treeNodeList.get(1);
	}

	public static void printByLayer(TreeNode root) {
		LinkedList<TreeNode> q = new LinkedList<>();
		q.add(root);

		while (!q.isEmpty()) {
			LinkedList<TreeNode> curLayerQ = new LinkedList<>();

			while (!q.isEmpty()) {
				TreeNode cur = q.pollFirst();
				if (cur == null){
					// 用于打印空元素
					cur = new TreeNode().setVal(null);
				}
				curLayerQ.add(cur.getLeft());
				curLayerQ.add(cur.getRight());

				System.out.print( (cur.getVal() == null ? "N" : cur.getVal()) + " ");
			}
			q.addAll(curLayerQ);
			if (q.stream().allMatch(Objects::isNull)){
				break;
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}


	public Object getVal() {
		return val;
	}

	public TreeNode setVal(Object val) {
		this.val = val;
		return this;
	}

	public TreeNode getLeft() {
		return left;
	}

	public TreeNode setLeft(TreeNode left) {
		this.left = left;
		return this;
	}

	public TreeNode getRight() {
		return right;
	}

	public TreeNode setRight(TreeNode right) {
		this.right = right;
		return this;
	}

	public TreeNode getParent() {
		return parent;
	}

	public TreeNode setParent(TreeNode parent) {
		this.parent = parent;
		return this;
	}
}
