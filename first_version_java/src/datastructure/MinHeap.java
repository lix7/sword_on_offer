package datastructure;

/**
 * Created by lix7 on 2019/2/28
 */
public class MinHeap {
	private int capacity;
	private int curIndex;
	private int[] data;

	public MinHeap(){
		this(Integer.MAX_VALUE);
	}

	public MinHeap(int capacity){
		this.capacity = capacity;
		curIndex = 1;
		this.data = new int[capacity+1];   // the index 0 is for placeholder
	}
	public int getCapacity(){
		return this.capacity;

	}

	public boolean isEmpty(){
		return size() == 0;
	}

	public int size(){
		return curIndex - 1;
	}

	public void add(int val) throws Exception {
		if (curIndex > capacity){
			throw new Exception("Heap is full!");
		}

		data[curIndex] = val;
		soar(curIndex);
		++curIndex;
	}

	public int getTop() throws Exception {
		if (isEmpty()){
			throw new Exception("Empty heap!");
		}

		int lastNumIndex = curIndex-1;

		int rtn = data[1];
		data[1] = data[lastNumIndex];
		--curIndex;

		sink(1);

		return rtn;
	}

	public int topVal() throws Exception {
		if (isEmpty()){
			throw new Exception("Empty heap!");
		}

		return data[1];
	}

	private void sink(int index){
		if (index < curIndex){
			int curVal = data[index];
			int leftVal = Integer.MAX_VALUE, rightVal = Integer.MAX_VALUE;
			if (getLeftChildIndex(index) < curIndex){
				leftVal = data[getLeftChildIndex(index)];
			}
			if (getRightChildIndex(index) < curIndex){
				rightVal = data[getRightChildIndex(index)];
			}

			int minIndex = -1;

			if (leftVal < rightVal){
				minIndex = getLeftChildIndex(index);
			}else {
				minIndex = getRightChildIndex(index);
			}

			if (minIndex < curIndex && curVal > data[minIndex]){
				swap(data, index, minIndex);
				sink(minIndex);
			}else {
				return;
			}

		}
	}

	private void soar(int index){
		if (index > 1){
			if (data[index] < data[getParentIndex(index)]){
				swap(data, index, getParentIndex(index));
				soar(getParentIndex(index));
			}
		}
	}

	private int getLeftChildIndex(int index){
		return 2*index;
	}

	private int getRightChildIndex(int index){
		return 2*index+1;
	}

	private int getParentIndex(int index){
		return index/2;
	}

	private void swap(int[] arr, int indexA, int indexB){
		int tmp = arr[indexA];
		arr[indexA] = arr[indexB];
		arr[indexB] = tmp;
	}

}
