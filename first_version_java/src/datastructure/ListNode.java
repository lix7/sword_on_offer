package datastructure;

import java.util.List;

/**
 * Created by lix7 on 2019/2/16
 */
public class ListNode {
	private Object val;
	private ListNode next;

	public static ListNode fromList(List<?> list){
		if (list == null || list.isEmpty()){
			return null;
		}

		if (list.size() == 1){
			return new ListNode().setVal(list.get(0)).setNext(null);
		}

		ListNode head = new ListNode();
		ListNode cur = head;
		int index = -1;
		for (Object element: list){
			++index;

			if (index == 0){
				head.setVal(element).setNext(new ListNode());
				cur = head.next;
			}else if (index == list.size()-1){
				cur.setVal(element).setNext(null);
			}else {
				cur.setVal(element).setNext(new ListNode());
				cur = cur.next;
			}
		}

		return head;
	}

	public void print() {
		ListNode cur = this;
		while(cur != null){
			System.out.print(cur.val + " ");
			cur = cur.next;
		}
		System.out.println("");
	}

	public ListNode get(int n){
		ListNode cur = this;
		for (int i = 0; i < n; ++i){
			cur = cur.getNext();
		}
		return cur;
	}

	public Object getVal() {
		return val;
	}

	public ListNode setVal(Object val) {
		this.val = val;
		return this;
	}

	public ListNode getNext() {
		return next;
	}

	public ListNode setNext(ListNode next) {
		this.next = next;
		return this;
	}

	@Override
	public String toString() {
		if (this.val instanceof Integer){
			return String.valueOf(this.val);
		}

		return this.val.toString();
	}
}
