import datastructure.TreeNode;

/**
 * Created by lix7 on 2019/2/22
 */
public class Quiz18IsSubtree {
	public static void main(String[] args) {
		TreeNode tree = new TreeNode()
				.setVal(1)
				.setLeft(
						new TreeNode()
								.setVal(2)
								.setRight(
										new TreeNode()
												.setVal(3)
								)
				)
				.setRight(
						new TreeNode()
								.setVal(4)
								.setLeft(
										new TreeNode()
												.setVal(6)
								)
								.setRight(
										new TreeNode()
												.setVal(7)
								)
				);

		TreeNode.printByLayer(tree);
		System.out.println("---------------");

		System.out.println(isSubtree(tree, null));      // true
		System.out.println(isSubtree(tree, tree.getLeft()));    // true
		System.out.println(isSubtree(tree, tree.getRight()));   // true
		System.out.println(isSubtree(tree, new TreeNode().setVal(2).setRight(new TreeNode().setVal(3))));   // true

		System.out.println(isSubtree(tree, new TreeNode().setVal(100)));    // false
	}


	public static boolean isSubtree(TreeNode tree, TreeNode subtree) {
		// note 注意边界条件和null判断

		if (tree == null && subtree == null) {
			return true;
		} else if (tree != null && subtree != null && tree.getVal() == subtree.getVal()) {
			// todo 这个判断条件有问题
			return isSubtree(tree.getLeft(), subtree.getLeft()) && isSubtree(tree.getRight(), subtree.getRight());
		} else if (tree != null){
			return isSubtree(tree.getLeft(), subtree) || isSubtree(tree.getRight(), subtree);
		} else {
			return false;
		}
	}


}
