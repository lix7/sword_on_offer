import datastructure.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz25TreePathSum {
	public static void main(String[] args){
		System.out.println(findSum(TreeNode.fromList(Arrays.asList(10,5,12,4,7)),new ArrayList<>(), 9));
		System.out.println(findSum(TreeNode.fromList(Arrays.asList(10,5,12,4,7)),new ArrayList<>(), 22));
		System.out.println(findSum(TreeNode.fromList(Arrays.asList(10,5,12,4,7)),new ArrayList<>(), 12));

		System.out.println(findSum(TreeNode.fromList(Arrays.asList(10,5,12,4,7)),new ArrayList<>(), 0));
		System.out.println(findSum(TreeNode.fromList(Arrays.asList(10,5,12,4,7)),new ArrayList<>(), 17));
	}

	public static boolean findSum(TreeNode root, List<TreeNode> exist, int num){
		if (root ==  null){
			return false;
		}

		exist.add(root);

		int[] existSum = new int[exist.size()+1];
		for (int i = exist.size()-1; i >= 0; --i){
			existSum[i] = existSum[i+1] + (int)exist.get(i).getVal();
		}
		for (int i = 0; i < exist.size(); ++i){
			if (existSum[i] == num){
				for (int j = i; j < exist.size(); ++j){
					System.out.print(exist.get(j).getVal() + " ");
				}

				return true;
			}
		}

		boolean subTreeFindRtn = findSum(root.getLeft(), exist, num) || findSum(root.getRight(), exist, num);
		exist.remove(exist.size()-1);

		return subTreeFindRtn;
	}

}
