import datastructure.TreeNode;

import java.util.Arrays;

/**
 * Created by lix7 on 2019/3/11
 */
public class Quiz39TreeDepth {

	public static void main(String[] args){
		TreeNode root = TreeNode.fromList(Arrays.asList(1,2,3,4,5,6,7,8,9,10));
		System.out.println(treeDepth(root, 0));
	}

	public static int treeDepth(TreeNode root, int curDepth){
		if (root == null){
			return curDepth;
		}else {
			return Math.max(treeDepth(root.getLeft(), curDepth+1), treeDepth(root.getRight(), curDepth+1));
		}
	}

}
