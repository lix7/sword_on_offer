import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

/**
 * Created by lix7 on 2019/2/25
 */
public class Quiz28AllSeqs {

	public static void main(String[] args){
		String chars = "abcd";

		printAllSeqs(chars, new ArrayList<>());

		printAllSeqsWithoutExtraSpace("abcd".toCharArray(), 0);
	}

	public static void printAllSeqs(String chars, ArrayList<Character> exist){
		// note 全排列

		if (chars == null){
			return;
		}

		for (int i = 0; i < chars.length(); ++i){
			char c = chars.charAt(i);
			if (!exist.contains(c)){
				exist.add(c);
				printAllSeqs(chars, exist);
				exist.remove(exist.size()-1);
			}
		}

		if (exist.size() == chars.length()){
			for (char c: exist){
				System.out.print(c+" ");
			}
			System.out.println();
		}
	}

	public static void printAllSeqsWithoutExtraSpace(char[] chars, int curIndex){
		// note 书上的方法，不需要额外空间而且效率高！

		if (curIndex == chars.length-1){
			for (char c: chars){
				System.out.print(c);
			}
			System.out.println("");
		}else {

			for (int i = curIndex; i < chars.length; ++i){

				char tmp = chars[i];
				chars[i] = chars[curIndex];
				chars[curIndex] = tmp;

				printAllSeqsWithoutExtraSpace(chars, curIndex+1);

				tmp = chars[i];
				chars[i] = chars[curIndex];
				chars[curIndex] = tmp;
			}

		}

	}

}
