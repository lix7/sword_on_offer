import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by lix7 on 2019/3/3
 */
public class Quiz32CountOneRangeOneToN {
	public static void main(String[] args){
		System.out.println(0 + "\t" + count(0) + "\t" + plainCount(0));   // 0
		System.out.println(1 + "\t" + count(1) + "\t" + plainCount(1));   // 1
		System.out.println(2 + "\t" + count(2) + "\t" + plainCount(2));   // 1
		System.out.println(10 + "\t" + count(10) + "\t" + plainCount(10));  // 2
		System.out.println(12 + "\t" + count(12) + "\t" + plainCount(12));  // 5
		System.out.println(20 + "\t" + count(20) + "\t" + plainCount(20));  // 12
		System.out.println(30 + "\t" + count(30) + "\t" + plainCount(30));  // 13
		System.out.println(99 + "\t" + count(99) + "\t" + plainCount(99));
		System.out.println(100 + "\t" + count(100) + "\t" + plainCount(100));
		System.out.println(200 + "\t" + count(200) + "\t" + plainCount(200));
		System.out.println(12345 + "\t" + count(12345) + "\t" + plainCount(12345));
	}


	public static int plainCount(int n){
		int cnt = 0;
		for (int i = 0; i <= n; ++i){
			cnt += plainCountOneNumber(i);
		}
		return cnt;
	}

	private static int plainCountOneNumber(int number){
		int rtn = 0;
		while (number != 0){
			if (number % 10 == 1){
				rtn += 1;
			}
			number /= 10;
		}
		return rtn;
	}

	public static int count(int num){
		return countImpl(Arrays.stream(String.valueOf(num).split("")).map(Integer::valueOf).collect(Collectors.toList()));
	}

	private static int countImpl(List<Integer> num){
		// note 自己想没想出来的方法，通过递归不断地剔除最高位即可，因为较小的数字所包含的1也一定会出现在更大的数字中

		if (num.size() <= 0){
			return 0;
		}

		if (num.size() == 1){
			return num.get(0) >= 1 ? 1 : 0;
		}

		int rtn = 0;
		for (int i = 0; i <= num.get(0); ++i){
			if (i == 1 && i == num.get(0)) {
				ArrayList<Integer> curNumList = new ArrayList<>(num);
				curNumList.set(0, 1);
				rtn += countNum(curNumList) + countImpl(num.subList(1, num.size()));
			}else if (i == 1 && i != num.get(0)) {
				ArrayList<Integer> curNumList = new ArrayList<>(num);
				for (int k = 1; k < curNumList.size(); ++k){
					curNumList.set(k, 9);
				}
				curNumList.set(0, 1);

				rtn += countNum(curNumList) + countImpl(curNumList.subList(1, num.size()));
			}else if (i == num.get(0)){
				rtn += countImpl(num.subList(1, num.size()));
			}else {
				List<Integer> curNumList = new ArrayList<>();
				for (int k = 0; k < num.size()-1; ++k){
					curNumList.add(9);
				}
				rtn += countImpl(curNumList);
			}
		}

		return rtn;
	}

	private static int countNum(List<Integer> numList){
		int rtn = 0;
		for (int digital: numList){
			rtn = rtn*10 + digital;
		}
		return rtn - (int)Math.pow(10, numList.size()-1) + 1;
	}

}
