import datastructure.ListNode;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by lix7 on 2019/2/16
 */
public class Quiz5PrintLinkedListReversely {
	public static void main(String[] args){
		List<Integer> list = new LinkedList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);

		ListNode head = ListNode.fromList(list);
		reversePrintByRecursive(head);
	}

	private static void reversePrintByStack(ListNode head) throws Exception {
		// 最基本的，就不实现了
		throw new Exception("Not implemented method");
	}

	private static void reversePrintByRecursive(ListNode head){
		// note 递归本质就是调用栈，所以两者是可以相互转化的；但是递归因为调用栈的原因效率比循环低并且容易栈溢出
		if (head == null){
			return;
		}

		reversePrintByRecursive(head.getNext());
		System.out.print(head.getVal() + " ");
	}


}
