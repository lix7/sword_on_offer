import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by lix7 on 2019/3/5
 */
public class Quiz35CharOccurredOnce {
	public static void main(String[] args){
		System.out.println(charOccurredOnce("a"));
		System.out.println(charOccurredOnce("aadccb"));
		System.out.println(charOccurredOnce("abcd"));
		System.out.println(charOccurredOnce("aabcdd"));
		System.out.println(charOccurredOnce("aabbccd"));
	}

	static Character charOccurredOnce(String str){
		if (str == null){
			return null;
		}

		Map<Character, Integer> m = new LinkedHashMap<>();
		for (char c: str.toCharArray()){
			if (m.containsKey(c)){
				m.put(c, m.get(c)+1);
			}else {
				m.put(c, 1);
			}
		}

		for (char c: m.keySet()){
			if (m.get(c) == 1){
				return c;
			}
		}

		return null;
	}

}
