/**
 * Created by lix7 on 2019/2/17
 */
public class Quiz9Fibonacci {
	public static void main(String[] args){
		for (int i = 1; i < 100; ++i){
			System.out.println(fibonacci(i));
		}
	}

	public static long fibonacci(int n){
		// n过大会溢出
		if (n == 1 || n == 2){
			return 1;
		}

		long prev2 = 1, prev = 1;
		long rtn = 0;
		for (int i = 2; i < n; ++i){
			rtn = prev + prev2;
			prev2 = prev;
			prev = rtn;
		}
		return rtn;
	}
}
