import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by lix7 on 2019/3/5
 */
public class Quiz34FindUglyNumber {
	public static void main(String[] args){
		System.out.println(findUglyNumber_(1500));
	}

	static int findUglyNumber(int k){
		// note 自己的解法，每次都要插入排序，不如书上的方法效率高，但是已经比暴力搜索好很多了
		int[] baseNums = new int[]{2,3,5};

		TreeSet<Integer> rtn = new TreeSet<>();
		rtn.add(1);
		rtn.add(2);
		rtn.add(3);
		rtn.add(5);

		int cur = 2;
		while (true){
			cur = rtn.ceiling(cur);

			if (rtn.subSet(1, true, cur*2, true).size() >= k){
				// 检查乘2后的结果是不是已经超过k个数，如果超过了，就可以返回了
				return rtn.subSet(1, true, cur*2, true).last();
			}

			for (int num: baseNums){
				rtn.add(cur*num);
			}

			++cur;
		}
	}


	static int findUglyNumber_(int k){
		// note 书上解法，很L厉害，当时做这道题时候钻牛角尖了，详细解释看书
		if (k <= 0){
			return 0;
		}

		int[] rtn = new int[k+1];
		rtn[1] = 1;
		int maxIndex = 1;
		int t2=1,t3=1,t5=1;


		while (maxIndex < k){
			int curMax = rtn[maxIndex];
			while (rtn[t2]*2 <= curMax){
				++t2;
			}
			while (rtn[t3]*3 <= curMax){
				++t3;
			}
			while (rtn[t5]*5 <= curMax){
				++t5;
			}

			rtn[++maxIndex] = min(rtn[t2]*2, rtn[t3]*3, rtn[t5]*5);
		}


		return rtn[k];
	}

	private static int min(int ...nums){
		int min = Integer.MAX_VALUE;
		for (int num : nums){
			if (num < min){
				min = num;
			}
		}
		return min;
	}

}
