import java.util.LinkedList;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz21StackWithMin {

	public static void main(String[] args) throws Exception {
		MinStack minStack = new MinStack();

		minStack.push(10);
		minStack.push(5);
		minStack.push(4);
		minStack.push(3);
		minStack.push(3);
		System.out.println(minStack.getMin());  // 3
		minStack.push(2);
		System.out.println(minStack.getMin());  // 2
		minStack.pollLast();    // 2
		System.out.println(minStack.getMin());  // 3
		minStack.pollLast();    // 3
		System.out.println(minStack.getMin());  // 3
		minStack.pollLast();    // 3
		System.out.println(minStack.getMin());  // 4


	}
}

class MinStack{

	private LinkedList<Integer> dataStack = new LinkedList<>();
	private LinkedList<Integer> minStack = new LinkedList<>();

	public void push(int data){
		dataStack.add(data);

		// note 一定注意为minStack为空时！
		if (minStack.isEmpty() || data <= getMin()){
			minStack.add(data);
		}

	}

	public int pollLast() throws Exception {
		if (dataStack.isEmpty()){
			throw new Exception("No data in stack");
		}

		int rtn = dataStack.pollLast();

		if (rtn == getMin()){
			minStack.pollLast();
		}

		return rtn;
	}

	public int getMin(){
		return minStack.getLast();
	}

}