import util.Utils;

import java.util.*;

/**
 * Created by lix7 on 2019/3/13
 */
public class Quiz40NumberAppearOnce {

	public static void main(String[] args){
//		System.out.println(getNumberAppearOnce(Utils.toIntegerList(new int[]{1,1,2,3,3})));
		System.out.println(getTwoNumberAppearOnce(Utils.toIntegerList(new int[]{1,2})));
		System.out.println(getTwoNumberAppearOnce(Utils.toIntegerList(new int[]{1,1,2,2,3,4,5,5,6,6})));
		System.out.println(getTwoNumberAppearOnce(Utils.toIntegerList(new int[]{1,2,2,3,3,4,5,5,6,6})));
		System.out.println(getTwoNumberAppearOnce(Utils.toIntegerList(new int[]{1,2,2,3,3,4,4,5,5,6})));
	}


	static List<Integer> getTwoNumberAppearOnce(List<Integer> arr){
		// note 还是快排的分区思想
		// 有两个重复数字，找出来

		int xorRes = 0;
		for (int num: arr){
			xorRes ^= num;
		}

		int mark = 1;
		while ((xorRes & 1) == 0){
			mark <<= 1;
			xorRes >>>= 1;
		}

		// divide arr into 2 parts
		int i = 0, j = arr.size()-1;
		while (true){
			while ( i < arr.size() && (arr.get(i) & mark) == 0){
				++i;
			}
			while ( j > 0 && (arr.get(j) & mark) != 0){
				--j;
			}

			if (i > j){
				break;
			}

			Utils.swapInList(arr, i, j);
		}

		List<Integer> rtn = new ArrayList<>();
		rtn.add(getNumberAppearOnce(arr.subList(0, j+1)));
		rtn.add(getNumberAppearOnce(arr.subList(j+1, arr.size())));

		return rtn;
	}

	private static int getNumberAppearOnce(List<Integer> arr){
		// 只有一个数字重复的数组

		int rtn = 0;
		for (int num: arr){
			rtn ^= num;
		}

		return rtn;
	}

}
