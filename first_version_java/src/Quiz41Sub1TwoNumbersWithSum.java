import util.Utils;

import java.util.List;

/**
 * Created by lix7 on 2019/3/21
 */
public class Quiz41Sub1TwoNumbersWithSum {

	public static void main(String[] args){
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,2,3,4,5}), 10));
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,2,3,4,5}), 0));
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,2,3,4,5}), 3));
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,2,3,4,5}), 6));
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,2,3,4,5}), 8));
		System.out.println(twoNumbersWithSum(Utils.toIntegerList(new int[]{1,3,5,7,10}), 14));
	}


	public static List<Integer> twoNumbersWithSum(List<Integer> nums, int sum){
		int i = 0, j = nums.size()-1;

		while (true){
			if (i >= j){
				return null;
			}

			int curSum = nums.get(i) + nums.get(j);

			if (curSum == sum){
				return Utils.toIntegerList(new int[]{nums.get(i), nums.get(j)});
			}

			if (curSum > sum){
				--j;
			}else {
				++i;
			}
		}
	}


}
