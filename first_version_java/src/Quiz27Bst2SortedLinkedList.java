import datastructure.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by lix7 on 2019/2/24
 */
public class Quiz27Bst2SortedLinkedList {
	public static void main(String[] args){
		TreeNode root = TreeNode.fromList(
				Arrays.asList(10, 6, 14, 4, 8, 12)
		);

		List<TreeNode> res = bst2SortedLinkedList(root);
		TreeNode head = res.get(0);
		while (head != null){
			System.out.print(head.getVal() + " ");
			head = head.getRight();
		}
	}

	public static List<TreeNode> bst2SortedLinkedList(TreeNode root){
		// note 注意处理叶节点的空指针，尤其是非完全二叉树

		if (root == null){
			// 注意此时应该返回两个null
			return new ArrayList<>(Arrays.asList(null, null));
		}

		if (root.getLeft() == null && root.getRight() == null){
			return new ArrayList<>(Arrays.asList(root, root));  // 首尾节点均为root
		}

		List<TreeNode> left = bst2SortedLinkedList(root.getLeft());
		List<TreeNode> right = bst2SortedLinkedList(root.getRight());

		TreeNode leftHead = left.get(0);
		TreeNode leftTail = left.get(1);
		TreeNode rightHead = right.get(0);
		TreeNode rightTail = right.get(1);

		if (leftTail != null)
			leftTail.setRight(root);

		root.setLeft(leftTail);
		root.setRight(rightHead);

		if (rightHead != null)
			rightHead.setLeft(root);

		return new ArrayList<>(Arrays.asList(leftHead, rightTail));
	}

}
